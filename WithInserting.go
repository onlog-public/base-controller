package base_controller

import (
	"context"
	"database/sql"
	. "gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2/types"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithInserting реализует контроллер Inserting.
type WithInserting[T Entity] struct {
	Repository Insert
}

// NewWithInserting реализует конструктор контроллера Inserting.
// На вход требует интерфейс для работы со вставкой сущностей.
func NewWithInserting[T Entity](repository Insert) *WithInserting[T] {
	return &WithInserting[T]{
		Repository: repository,
	}
}

// InsertByGraphQlParameters выполняет вставку значений по переданным
// параметрам GraphQL
func (w WithInserting[T]) InsertByGraphQlParameters(
	ctx context.Context,
	params types.Parameters,
) (any, error) {
	return w.Repository.InsertByGraphQlParameters(params, h.ContextValue[*sql.Tx](ctx))
}
