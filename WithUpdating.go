package base_controller

import (
	"context"
	"database/sql"
	. "gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2/types"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithUpdating реализует контроллер Updating
type WithUpdating[T Entity] struct {
	Repository Update
}

// NewWithUpdating реализует конструктор контроллера Updating.
// На вход требуется передать репозиторий, реализующий обновление сущностей.
func NewWithUpdating[T Entity](repository Update) *WithUpdating[T] {
	return &WithUpdating[T]{
		Repository: repository,
	}
}

// UpdateByGraphQlParameters выполняет обновление значений по переданным
// параметрам GraphQL
func (w WithUpdating[T]) UpdateByGraphQlParameters(
	ctx context.Context,
	params types.Parameters,
) (any, error) {
	return w.Repository.UpdateByGraphQlParameters(params, h.ContextValue[*sql.Tx](ctx))
}
