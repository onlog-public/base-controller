package base_controller

import (
	"context"
	"database/sql"
	. "gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2/types"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithDeleting реализует контроллер Deleting.
type WithDeleting[T Entity] struct {
	Repository Delete
}

// NewWithDeleting реализует конструктор контроллера Deleting.
// Требует на вход репозиторий, реализующий удаление сущностей.
func NewWithDeleting[T Entity](repository Delete) *WithDeleting[T] {
	return &WithDeleting[T]{
		Repository: repository,
	}
}

// DeleteByGraphQlParameters выполняет удаление значений по переданным
// параметрам GraphQL
func (w WithDeleting[T]) DeleteByGraphQlParameters(
	ctx context.Context,
	params types.Parameters,
) (any, error) {
	return w.Repository.DeleteByGraphQlParameters(params, h.ContextValue[*sql.Tx](ctx))
}
