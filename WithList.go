package base_controller

import (
	"context"
	"database/sql"
	. "gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2/types"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithList реализует контроллер List
type WithList[T Entity] struct {
	Repository Getter[T]
}

// NewWithList реализует конструктор контроллера List.
// Принимает на вход репозиторий для работы с сущностями.
func NewWithList[T Entity](repository Getter[T]) *WithList[T] {
	return &WithList[T]{
		Repository: repository,
	}
}

// GetListByGraphQlParameters возвращает листинг сущностей по
// переданным параметрам GraphQL
func (w WithList[T]) GetListByGraphQlParameters(
	ctx context.Context,
	params types.Parameters,
) (any, error) {
	return w.Repository.GetListByGraphQlParameters(params, h.ContextValue[*sql.Tx](ctx))
}
