package base_controller

import (
	"context"
	"database/sql"
	. "gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2/types"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// WithAggregate реализует контроллер Aggregate
type WithAggregate[T Entity] struct {
	Repository Aggregator
}

// NewWithAggregate реализует конструктор контроллера Aggregate.
// Принимает на вход репозиторий для работы с агрегацией сущностей.
func NewWithAggregate[T Entity](repository Aggregator) *WithAggregate[T] {
	return &WithAggregate[T]{
		Repository: repository,
	}
}

// GetAggregationByGraphQlParameters возвращает результаты агрегации
// сущностей по переданным параметрам GraphQL
func (w WithAggregate[T]) GetAggregationByGraphQlParameters(
	ctx context.Context,
	params types.Parameters,
) (any, error) {
	return w.Repository.GetAggregationByGraphQlParameters(params, h.ContextValue[*sql.Tx](ctx))
}
