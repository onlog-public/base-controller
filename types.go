package base_controller

import (
	"context"
	. "gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2/types"
)

// List описывает интерфейс контроллера с методом листинга сущностей
// на основе PgRepo
type List[T Entity] interface {
	// GetListByGraphQlParameters возвращает листинг сущностей по
	// переданным параметрам GraphQL
	GetListByGraphQlParameters(
		ctx context.Context,
		params types.Parameters,
	) (any, error)
}

// Aggregate описывает интерфейс контроллера с методом агрегации данных
// сущностей по переданным параметрам GraphQL
type Aggregate interface {
	// GetAggregationByGraphQlParameters возвращает результаты агрегации
	// сущностей по переданным параметрам GraphQL
	GetAggregationByGraphQlParameters(
		ctx context.Context,
		params types.Parameters,
	) (any, error)
}

// Inserting описывает интерфейс контроллера с методом вставки новых сущностей
type Inserting interface {
	// InsertByGraphQlParameters выполняет вставку значений по переданным
	// параметрам GraphQL
	InsertByGraphQlParameters(
		ctx context.Context,
		params types.Parameters,
	) (any, error)
}

// Updating описывает интерфейс контроллера с методом обновления сущностей
type Updating interface {
	// UpdateByGraphQlParameters выполняет обновление значений по переданным
	// параметрам GraphQL
	UpdateByGraphQlParameters(
		ctx context.Context,
		params types.Parameters,
	) (any, error)
}

// Deleting описывает интерфейс контроллера с методом удаления сущностей
type Deleting interface {
	// DeleteByGraphQlParameters выполняет удаление значений по переданным
	// параметрам GraphQL
	DeleteByGraphQlParameters(
		ctx context.Context,
		params types.Parameters,
	) (any, error)
}

// Reading описывает интерфейс контроллера с методами чтения сущностей.
// Совмещает в себе интерфейсы List и Aggregate.
type Reading[T Entity] interface {
	List[T]
	Aggregate
}

// Change описывает интерфейс контроллера с методами изменения сущностей.
// Совмещает в себе интерфейсы Inserting, Updating и Deleting.
type Change[T Entity] interface {
	Inserting
	Updating
	Deleting
}

// FullController описывает интерфейс контроллера, совмещающего в себе
// функционал чтения Reading и изменения Change.
type FullController[T Entity] interface {
	Reading[T]
	Change[T]
}
