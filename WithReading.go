package base_controller

import . "gitlab.com/onlog-public/base-repo"

// ReadingRepository описывает интерфейс репозитория,
// совмещающий функционал чтения сущностей и агрегации.
type ReadingRepository[T Entity] interface {
	Getter[T]
	Aggregator
}

// WithReading реализует контроллер Reading.
type WithReading[T Entity] struct {
	*WithList[T]
	*WithAggregate[T]
}

// NewWithReading реализует конструктор контроллера Reading.
// На вход требует репозиторий агрегации и чтения листинга сущностей.
func NewWithReading[T Entity](repository ReadingRepository[T]) *WithReading[T] {
	return &WithReading[T]{
		WithList:      NewWithList[T](repository),
		WithAggregate: NewWithAggregate[T](repository),
	}
}
