package base_controller

import (
	. "gitlab.com/onlog-public/base-repo"
)

// ChangeRepository описывает интерфейс репозитория изменения
// сущностей.
type ChangeRepository interface {
	Insert
	Update
	Delete
}

// WithChange реализует контроллер Change
type WithChange[T Entity] struct {
	*WithInserting[T]
	*WithUpdating[T]
	*WithDeleting[T]
}

// NewWithChange реализует конструктор контроллера Change.
// На вход требуется репозиторий, реализующий все методы изменения сущностей.
func NewWithChange[T Entity](repo ChangeRepository) *WithChange[T] {
	return &WithChange[T]{
		WithInserting: NewWithInserting[T](repo),
		WithUpdating:  NewWithUpdating[T](repo),
		WithDeleting:  NewWithDeleting[T](repo),
	}
}
