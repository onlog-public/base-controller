package base_controller

import . "gitlab.com/onlog-public/base-repo"

// Full реализует контроллер с полным функционалом чтения
// и изменения сущностей.
type Full[T Entity] struct {
	*WithReading[T]
	*WithChange[T]
}

// NewFull реализует конструктор контроллера, реализующий полный
// функционал работы с сущностями. Реализует интерфейс FullController.
func NewFull[T Entity](readRepo ReadingRepository[T], changeRepo ChangeRepository) *Full[T] {
	return &Full[T]{
		WithReading: NewWithReading[T](readRepo),
		WithChange:  NewWithChange[T](changeRepo),
	}
}
